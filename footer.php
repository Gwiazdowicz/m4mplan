<footer class="footer">
    <div class="footer__border border">
        <div class="footer__tile">
            <div class="footer__col-1">
                <div class="footer__box-one">
                    <div class="footer__box-title">Home</div>
                    <ul class="footer__nav">
                        <li><a href="#!" class="footer__nav-text">O nasz</a></li>
                        <li><a href="#!" class="footer__nav-text">Oferta</a></li>
                        <li><a href="#!" class="footer__nav-text">Realizacje</a></li>
                        <li><a href="#!" class="footer__nav-text">Zespół</a></li>
                        <li><a href="#!" class="footer__nav-text">Kursy</a></li>
                        <li><a href="#!" class="footer__nav-text">Kontakt</a></li>
                    </ul>
                </div>
                <div class="footer__box-two">
                    <div class="footer__contact">
                        <div class="footer__contact-tilte">Mam Plan - Projektowanie wnętrz</div>
                        <div class="footer__contact-data">
                            <div class="footer__contact-name">Paulina Gawron</div>
                            <div class="footer__contact-tel">Tel. 515 341 580 <br><span>E-mail: pg@m4mplan.pl</span></div>
                        </div>
                        <div class="footer__contact-data">
                            <div class="footer__contact-name">Renata Peron</div>
                            <div class="footer__contact-tel">Tel. 515 341 580 <br><span>E-mail: rp@m4mplan.pl</span></div>
                        </div>
                    </div>
                    <div class="footer__contact-media">
                        <a href="#!"> Instagram</a>
                        <div class="footer__med">/</div>
                        <a href="#!"> Facebook</a>
                    </div>
                </div>
                <div class="footer__box-three">
                    <div class="footer__bottom-text">2018 © Made with passion by <span>Time4</span></div>
                </div>
            </div>
            <div class="footer__col-2">
                <div class="footer__instagram-block">
                    <div class="footer__instagram-title">Instagram</div>
                    <div class="footer__instagram-lightbox">
                            <div class="footer__instagram-row1">
                                <div class="footer__img footer__img-1"></div>
                                <div class="footer__img footer__img-2"></div>
                                <div class="footer__img footer__img-4"></div>
                                <div class="footer__img footer__img-x"></div>
                            </div>
                            <div class="footer__instagram-row2">
                                <div class="footer__img footer__img-5"></div>
                                <div class="footer__img footer__img-6"></div>
                                <div class="footer__img footer__img-7"></div>
                                <div class="footer__img footer__img-x"></div>
                            </div>
                    </div>
                    <button class="footer__instagram-btn">do góry</button>
                </div> 
            </div>
        </div>
       
    </div>
</footer>

</main>
</body>
</html>
