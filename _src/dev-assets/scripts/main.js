$(document).ready(function() {
  $('.js__toogle-btn').click(function() {
    $('.js__toogle-content').slideToggle(2500);
    $(this).text(function(i, text) {
      return text === 'Pokaz mniej' ? 'Pokaz więcej' : 'Pokaz mniej';
    });
  });
});


var arr = ['Szeligowska 53 m2', 'Mickiewicza 60m2', 'Długa 47m2'];
var i = 0;
var textbox = document.getElementById('textbox');

// function nextItem() {
//   i = i + 1;
//   i = i % arr.length;
//   textbox.textContent=arr[i];
//   return arr[i];
// }//setInterval(nextItem, 1000);

// function prevItem() {
//   i = i + 2;
//   i = i % arr.length;
//   textbox.textContent=arr[i];
//   return arr[i];
// }

// window.addEventListener('load', function() {
//   textbox.textContent = arr[0];
//   document.getElementById('arrow-2').addEventListener('click', function(e) {
//     textbox.textContent = nextItem();
//   });
//   setTimeout(500);
//   document.getElementById('arrow-1').addEventListener('click', function(e) {
//     textbox.textContent = prevItem();
//   });
//   setTimeout(500);
// });


$('#slideoffert .slick').slick({
  slidesToShow: 6,
  dots: false,
  infinite: false,
  arrows: false,
  autoplay: true,
 
 
  responsive: [
    {
      breakpoint: 1620,
      settings: {
        slidesToShow: 4,
        slidesToScroll: 4,
        infinite: true,
        centerMode: true,
        centerPadding: '40px',
      }
    },

    {
      breakpoint: 970,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
        centerMode: true,
        centerPadding: '40px',
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        autoSpeed: 8000,
        speed: 3000,
        centerMode: true,
        centerPadding: '40px',
      }
    }
  ]
});
$('#slideshow .slick').slick({
  slidesToShow: 5,
  dots: false,
  arrows: false,
  autoplay: true,
  autoSpeed: 8000,
  speed: 3000,
  centerMode: true,
  centerPadding: '40px'
});

const $heroSlider = $('#slidehero .slick'); 
const $titlebox = $('.hero__title h1');

$('#slidehero .slick').slick({
  slidesToShow: 1,
  arrows: false,
  autoplay: true,
  autoSpeed: 8000,
  speed: 1000,
  dots:true,
  fade: true
});

$heroSlider.on('beforeChange', function(event, slick, currentSlide, nextSlide){
  const $nextSlideHero = $(slick.$slides[nextSlide]);
  const textHero = $nextSlideHero.find('.js-text-hero').text();
  $titlebox.text(textHero);
});

const $projectSlider = $('#slideproject .slick'); 
const $textbox = $('#textbox');

$projectSlider.slick({
  slidesToShow: 1,
  arrows: false,
  autoplay: true,
  autoSpeed: 8000,
  speed: 2000,
  dots: false,
  infinite: true,
  fade: true
});

$projectSlider.on('beforeChange', function(event, slick, currentSlide, nextSlide){
  const $nextSlide = $(slick.$slides[nextSlide]);
  const text = $nextSlide.find('.js-text').text();
  $textbox.text(text);
});


$('.project-arrow-left').click(function() {
  $('#slideproject .slick').slick('slickPrev');
});

$('.project-arrow-right').click(function() {
  $('#slideproject .slick').slick('slickNext');
});

$('.hero-arrow-left').click(function() {
  $('#slidehero .slick').slick('slickPrev');
});

$('.hero-arrow-right').click(function() {
  $('#slidehero .slick').slick('slickNext');
});


$('.offert-arrow-left').click(function() {
  $('#slideoffert .slick').slick('slickPrev');
});

$('.offert-arrow-right').click(function() {
  $('#slideoffert .slick').slick('slickNext');
});
