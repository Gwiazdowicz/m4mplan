/*
 * Prettier project-specific rules go here (if needed).
 **/

const baseConfig = require('./.prettierrc.base');

module.exports = {
  ...baseConfig
};
