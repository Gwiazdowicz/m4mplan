<section class="hero has-overlay">
            <div id="slidehero">
                <div class="slick slider">
                    <div class="background background-1">
                        <div class="js-text-hero" style="display:none">Skandynawski minimalizm</div>
                    </div>
                    <div class="background background-2">
                        <div class="js-text-hero" style="display:none">Lorem</div>
                    </div>
                    <div class="background background-3">
                        <div class="js-text-hero" style="display:none">Skandynawski minimalizm</div>
                    </div>
                </div>    
            </div>
    <div class="hero__content">
        <div class="hero__border borders">
            <div class="hero__container container">
                <div class="hero__text">
                    <div class="hero__title">
                        <h1>Skandynawski<br>minimalizm</h1>
                        <div class="hero__subititle-box">
                            <div class="hero__subtitle">Warsawa Marysin</div>
                        </div>
                    </div>
                    <div class="hero__arrows__tile">
                        <div class="hero__arrows">
                            <button href="#!" class="hero-arrow-left hero__arrow hero__arrow--left"></button>
                            <button href="#!" class="hero-arrow-right hero__arrow hero__arrow--right"></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>    
    </div>
</section>

<div class="slick hero__slider">
    <div class="hero__background"></div>
    <div class="hero__background"></div>
</div>

