<section class="course">
  <div class="course__border border">
    <div class="course__container container">
        
        <div class="course__col-1">

            <div class="course__sidebar">
                <ul class="course__sidebar-swich">
                    <li class="course__sidebar-btn-box" ><a href="#!" class="course__sidebar-btn">Wykładowcy</a></li>
                    <li class="course__sidebar-btn-box" ><a href="#!" class="course__sidebar-btn">Projektowanie wntrz</a></li>
                    <li class="course__sidebar-btn-box" ><a href="#!" class="course__sidebar-btn">pCoN Planner</a></li>
                    <li class="course__sidebar-btn-box" ><a href="#!" class="course__sidebar-btn">Fotografia wnętrz</a></li>
                    <li class="course__sidebar-btn-box" ><a href="#!" class="course__sidebar-btn">Home Staging</a></li>
                    <li class="course__sidebar-btn-box" ><a href="#!" class="course__sidebar-btn">Terminy kursów</a></li>
                </ul>
                <div class="course__sidebar-submit">
                    <a href="#!" class="course__resrvation-btn">Zarejestruj się</a> 
                </div>
            </div>
        </div>


        <div class="course__col-2">

            <div class="course__hours course__box-1">
                <div class="course__hours-tile">
                    <div class="course__hours-number">35 h</div>
                    <div class="course__hours-description">
                        <div class="course__hours-description-text course__hours-description--bottom">wykładów</div>
                    </div>
                </div>
                <div class="course__hours-tile">
                    <div class="course__hours-number">40 h</div>
                    <div class="course__hours-description">
                        <div class="course__hours-description-text course__hours-description--bottom">ćwiczeń</div>
                    </div>
                </div>
                <div class="course__hours-tile">
                    <div class="course__hours-number">30 h</div>
                    <div class="course__hours-description">
                        <div class="course__hours-description-text course__hours-description--bottom">zajęć komputerowych</div>
                    </div>
                </div>
            </div>

            <div class="course__banner course__box-2">
                <div class="course__banner-text">Uczymy <span>bezpłatnego programu</span> do projektowania wnętrz</div>
                <div class="course__banner-subtitle">Organizujemy spotkania branżowe w showroomach naszych partnetów.</div>
            </div>

            <div class="course__about course__box-3">
                <div class="course__about-text">
                    <div class="course__about-title">O kursie</div>
                    <div class="course__about-subtitle">Nauczysz się jak zaprojektować wnętrze od inwentaryzacji, przez stworzenie koncepcji, dokumentacji, po kosztorys 
                    i prezentację. Opracujesz swój pierwszy projekt pod okiem architektów i projektantów 
                    z wieloletnim doświadczeniem.</div>
                </div>
                <div class="course__about-info">
                    <div class="course__about-discription course__about-discription--grey">Czas trwania kursu:</div>
                    <div class="course__about-discription"><b>105 h</b> (35 h wykładów, 40 h ćwiczeń, 30 h zajęć komputerowych).</div>
                    <div class="course__about-discription  course__about-discription--grey">Limit miejsc:</div>
                    <div class="course__about-discription">15</div>
                    <div class="course__about-discription  course__about-discription--grey">Prowadzący:</div>
                    <div class="course__about-discription course__about-discription--bold">mgr. inż. Izabela Gemzała, mgr. inż. Elwira Kamińska-Citko,  Artur Brzuchacz,inż. Paulina Gawron</div>
                </div>
            </div>

            <div class="course__show-more course__box-4">
                <div class="course__show-more-box">
                    <a href="#!" class="course__show-more-btn">Pokaż program</a>
                </div>
            </div>
        
        <div class="course__block">
                <div class="course__cost course__box-5">
                    <div class="course__cost-title">Koszt</div>
                    <div class="course__cost-box">
                        <div class="course__cost-discription">Projektowanie wnętrz + pCon planner </div>
                        <div class="course__cost-price">3 399 zł / 3 199 zł</div>
                        <div class="course__sale">Niższa cena przy rejestracji do końca lutego!</div>
                    </div>
                    <div class="course__cost-box">
                        <div class="course__cost-discription">Projektowanie wnętrz </div>
                        <div class="course__cost-price">2 499 zł / 2 399 zł</div>
                        <div class="course__sale">Niższa cena przy rejestracji do końca lutego!</div>
                    </div>
                    <div class="course__cost-box-info">
                        <div class="course__cost-info">Posiadamy wpis do rejestru instytucji szkoleniowych możesz uzyskać dofinansowanie z Urzędu Pracy
                        </div>
                    </div>
                    <div class="course__cost-box-contact">
                        <div class="course__cost-contact-title">Rejestracja i szczegółowe informacje</div>
                        <div class="course__cost-contact-data">Mailowo lub telefonicznie<br><span>kursy@m4mplan.pl</span><br> tel. 515 341 580</div>
                    </div>
                </div>


                <div class="course__term course__box-6">
                    <div class="course__term-title">Najbliższe terminy</div>
                        <div class="course__mob">
                            <div class="course__term-box">
                                <div class="course__term-name">Kurs projektowania wnętrz</div>
                                <div class="course__term-date">/ Start: 24.03.2018</div>
                            </div>
                            <div class="course__term-box">
                                <div class="course__term-name">Kurs projektowania wnętrz</div>
                                <div class="course__term-date">/ Start: 19.05.2018</div>
                            </div>
                            <div class="course__term-box">
                                <div class="course__term-name">Kurs projektowania wnętrz</div>
                                <div class="course__term-date">/ Start: 22.06.2018</div>
                            </div>
                            <div class="course__term-box">
                                <div class="course__term-name">Warsztaty fotografii wnętrz</div>
                                <div class="course__term-date">/ 24 -25.03.2018</div>
                            </div>
                            <div class="course__term-box">
                                <div class="course__term-name">Warsztaty fotografii wnętrz</div>
                                <div class="course__term-date">/19 -29.05.2018</div>
                            </div>
                            <div class="course__term-box">
                                <div class="course__term-name">Warsztaty fotografii wnętrz</div>
                                <div class="course__term-date">/19 -29.05.2018</div>
                            </div>
                            <div class="course__term-box">
                                <div class="course__term-name">Warsztaty fotografii wnętrz</div>
                                <div class="course__term-date">/19 -29.05.2018</div>
                            </div>
                            <div class="course__term-btn-box">
                                <a  href="#!" class="course__term-btn">Pobierz PDF</a>
                            </div>
                        </div>

                        <div class="course__term-desk">
                            <div class="course__term-desk-term"><span>24-25</span> marca 2018</div>
                            <div class="course__term-desk-term"><span>24-25</span> czerwca 2018</div>
                            <div class="course__term-desk-term"><span>24-25</span> maja 2018</div>
                            <div class="course__term-desk-term"><span>24-25</span> marca 2018</div>
                            <div class="course__term-desk-term"><span>24-25</span> marca 2018</div>
                        </div>
                </div>
        </div>
    </div>

    </div>
  </div>
</section>