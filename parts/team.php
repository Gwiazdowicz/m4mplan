
<section class="team js__toogle-content">
    <div class="team__border border">
        <div class="team__container">
                <div class="team__header offert__head">
                    <div class="offert__title">Zespół</div>
                    <div class="offert__paddle"></div>
                </div>
            <div class="team__box">
                <div class="team__tile-one">
                    <div class="team__img"></div>
                    <div class="team__name">Paulina Gawron</div>
                    <div class="team__about">Inżynier, projektant wnętrz, visual 
                    merchandiser i wykładowca kierunku dekorator wnętrz. Fascynuje mnie 
                    oświetlenie projektowane przez Karima Rashid, choć na co dzień bliżej mi do industrialnych wnętrz. Pracowałam 
                    dla marek takich jak Mattel czy Miloo Home [House & More].</div>
                </div>
                <div class="team__tile-two">
                    <div class="team__img"></div>
                    <div class="team__name">Dorota Kotz</div>
                    <div class="team__about">Inżynier, projektant wnętrz, visual 
                    merchandiser i wykładowca kierunku dekorator wnętrz. Fascynuje mnie 
                    oświetlenie projektowane przez Karima Rashid, choć na co dzień bliżej mi do industrialnych wnętrz. Pracowałam 
                    dla marek takich jak Mattel czy Miloo Home [House & More].</div>
                </div>
                <div class="team__tile-three">
                    <div class="team__img"></div>
                    <div class="team__name">Kamila Rykowska</div>
                    <div class="team__about">Inżynier, projektant wnętrz, visual 
                    merchandiser i wykładowca kierunku dekorator wnętrz. Fascynuje mnie 
                    oświetlenie projektowane przez Karima Rashid, choć na co dzień bliżej mi do industrialnych wnętrz. Pracowałam 
                    dla marek takich jak Mattel czy Miloo Home [House & More].</div>
                </div>
            </div>
        </div>
    </div>
</section>


