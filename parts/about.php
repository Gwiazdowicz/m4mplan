<section>
    <div class="about">
        <div class="about__border border">
            <div class="about__container container">
                <div class="about__tile">
                    <div class="about__aside-element">
                        <span>O NAS</span>
                    </div>
                    
                    <div class="about__box-one">
                        <div class="about__box-content">
                            <div class="about__title a-article-title">„Architektura to również sztuka komunikacji”.</div>
                            <div class="about__subtitle a-article-subtitle">DANIEL LIBESKIND</div>
                        </div>
                        <div class="about__box-img">
                            <div class="about__box-one-img has-overlay"></div>
                        </div>
                    </div>
                    
                    <div class="about__box-two">
                        <div class="about__box-two-title">
                            <div class="about__text-bold a-text-bold">"Massa ac turpis faucibus orci luctus non, consectetuer lobortis quis, varius in, purus. Integer ultrices posuer”</div>
                            <div class="about__text-normal a-text-normal">Pellentesque facilisis. Nulla imperdiet sit amet magna. Vestibulum dapibus, mauris nec malesuada fames ac turpis velit, rhoncus eu, luctus et interdum adipiscing wisi. Aliquam erat ac ipsum. Integer aliquam purus. Quisque lorem tortor fringilla sed, vestibulum id, eleifend justo vel bibendum sapien massa ac turpis faucibus.</div>
                            <a href="#!" class="js__toogle-btn about__btn a-btn">Pokaz więcej</a>
                            <div class="about__text-normal-unshow a-text-normal">Pellentesque facilisis. Nulla imperdiet sit amet magna. Vestibulum dapibus, mauris nec malesuada fames ac turpis velit,</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>





