
<section class="partners">
    <div class="partners__border border">
        <div class="partners__container">
           <div class="partners__text-box">
                <div class="partners__text">Partnerzy</div>
           </div>
           <div class="partners__icons-box">
                <div class="partners__col-1">
                    <div class="partners__icon partners__icon--one"></div>
                    <div class="partners__icon partners__icon--two"></div>
                    <div class="partners__icon partners__icon--three"></div>
                    <div class="partners__icon partners__icon--four"></div>
                </div>
                <div class="partners__col-2">
                    <div class="partners__icon partners__icon--five"></div>
                    <div class="partners__icon partners__icon--six"></div>
                    <div class="partners__icon partners__icon--seven"></div>
                </div>
           </div>
           <div id="slideshow">
                <div class="slick">
                    <div class="partners__icon partners__icon--one"></div>
                    <div class="partners__icon partners__icon--two"></div>
                    <div class="partners__icon partners__icon--three"></div>
                    <div class="partners__icon partners__icon--four"></div>
                    <div class="partners__icon partners__icon--five"></div>
                    <div class="partners__icon partners__icon--six"></div>
                    <div class="partners__icon partners__icon--seven"></div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="box">
    <div class="left"></div>
    <div class="right"></div>
</div>



           