<section>
    <div class="offert">
        <div class="offert__border border">
            <div class="offert__container container">
                <div class="offert__tile">
                    <div class="offert__box-one">
                        <div class="offert__head">
                            <div class="offert__title">Oferta</div>
                            <div class="offert__paddle"></div>
                        </div>
                        <div class="offert__btns-offert">
                            <a href="#!" class="offert__btn-one a-btn-2 a-btn-2--black">Wnętrza prywatne</a>
                            <a href="#!" class="offert__btn-one a-btn-2 ">Wnętrza komercyjne</a>
                        </div>
                    </div>
                    
                    <div class="offert__box-two">
                        <div id="slideoffert">
                            <div class="slick">
                                <div class="offert__icon offert__icon--one">
                                    <div class="offert__icon-text">01</div>
                                    <div class="offert__icon-discription">Konsultacja</div>
                                </div>
                                <div class="offert__icon offert__icon--one">
                                    <div class="offert__icon-text">02</div>
                                    <div class="offert__icon-discription">Projekt koncepcyjny</div>
                                </div>
                                <div class="offert__icon offert__icon--one">
                                    <div class="offert__icon-text">03</div>
                                    <div class="offert__icon-discription">Wizualizacje wykończenia wnętrz</div>
                                </div>
                                <div class="offert__icon offert__icon--one">
                                    <div class="offert__icon-text">04</div>
                                    <div class="offert__icon-discription"> Dokumentacja techniczna dla wykonawców</div>
                                </div>
                                <div class="offert__icon offert__icon--one">
                                    <div class="offert__icon-text">05</div>
                                    <div class="offert__icon-discription">Kosztorys</div>
                                </div>
                                <div class="offert__icon offert__icon--one">
                                    <div class="offert__icon-text">06</div>
                                    <div class="offert__icon-discription">Nadzór</div>
                                </div>
                        </div>
                        </div>
                        <div class="offert__arrows">
                            <div class="offert__arrow-1 offert-arrow-left"></div>
                            <div class="offert__arrow-2 offert-arrow-right"></div>
                        </div>
                    </div>
                    
                    <div class="offert__box-three">
                        <div class="offert__offert">
                            <div class="offert__number"><p>03</p></div>
                            <div class="offert__title-article">Wizualizacje wykończenia wnętrz</div>
                        </div>
                        <div class="offert__discription">
                            <div class="offert__header"><p>Opis</p></div>
                            <div class="offert__text-bold a-text-bold">Faucibus orci luctus non, consectetuer lobortis quis, varius in, purus</div>
                            <div class="offert__text-normal a-text-normal">Nulla imperdiet sit amet magna. Vestibulum dapibus, mauris nec malesuada fames ac turpis velit, rhoncus eu, luctus et interdum adipiscing wisi. Aliquam erat ac ipsum. Integer aliquam purus. Quisque lorem tortor fringilla sed, vestibulum id, eleifend justo vel bibendum sapien massa ac turpis faucibus orci luctus non, consectetuer lobortis quis, varius in, purus. Integer ultrices posuere cubilia Curae, Nulla ipsum dolor lacus, suscipit adipiscing. Cum sociis natoque penatibus et ultrices volutpat. Nullam wisi ultricies a, gravida vitae dapibus.</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>