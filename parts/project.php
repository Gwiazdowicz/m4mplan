<section>
    <div class="project">
        <div class="project__banner has-overlay">
            <div id="slideproject">
                <div class="slick">
                    <div class="project__banner-1">
                        <div class="js-text" style="display:none">Szeligowska 53 m2</div>
                    </div>
                    <div class="project__banner-2">
                        <div class="js-text" style="display:none">Mickiewicza 60m2</div>
                    </div>
                    <div class="project__banner-3">
                        <div class="js-text" style="display:none">Długa 47m2</div>
                    </div>
                </div>    
            </div>
            <div class="project__border border">
                <div class="project__container container">
                    <div class="project__new">
                        <div class="project__new-title">Najnowsze Realizacje</div>
                        <div class="project__new-paddle"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="project__infobox">
            <div class="project__arrows">
                <div id="arrow-1" class="project-arrow-left project__arrow-1"></div>
                <div id="arrow-2" class="project-arrow-right project__arrow-2"></div>
            </div>
            <div class="project__change-arrows-text">
                <div id="textbox" class="project__title">Szeligowska 53 m2</div>
                <div class="project__show">
                    <div class="project__show-title">Zobacz więcej</div>
                    <div class="project__show-paddle"></div>
                </div>
            </div>
        </div>
    </div>
</section>